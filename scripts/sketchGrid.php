<?php
function getGridStructure($cols = 1)
{
    $sketchGrid = array();
    switch ($cols) {
        case 0:
            $sketchGrid["cols"] = "columns large-12 medium-12 small-12";
            $sketchGrid["row"] = 1;
            break;
        case 1:
            $sketchGrid["cols"] = "columns large-12 medium-12 small-12";
            $sketchGrid["row"] = 1;
            break;
        case 2:
            $sketchGrid["cols"] = "columns large-6 medium-6 small-12";
            $sketchGrid["row"] = 2;
            break;
        case 3:
            $sketchGrid["cols"] = "columns large-4 medium-12 small-12";
            $sketchGrid["row"] = 3;
            break;
        case 4:
            $sketchGrid["cols"] = "columns large-3 medium-6 small-12";
            $sketchGrid["row"] = 4;
            break;
        case 5:
            $sketchGrid["cols"] = "columns large-3 medium-6 small-12";
            $sketchGrid["row"] = 4;
            break;
        case 6:
            $sketchGrid["cols"] = "columns large-2 medium-4 small-6";
            $sketchGrid["row"] = 6;
            break;
        case 7:
            $sketchGrid["cols"] = "columns large-2 medium-4 small-6";
            $sketchGrid["row"] = 6;
            break;
        case 8:
            $sketchGrid["cols"] = "columns large-2 medium-4 small-6";
            $sketchGrid["row"] = 6;
            break;
        case 9:
            $sketchGrid["cols"] = "columns large-2 medium-4 small-6";
            $sketchGrid["row"] = 6;
            break;
        case 10:
            $sketchGrid["cols"] = "columns large-2 medium-4 small-6";
            $sketchGrid["row"] = 6;
            break;
        case 11:
            $sketchGrid["cols"] = "columns large-2 medium-4 small-6";
            $sketchGrid["row"] = 6;
            break;
        case 12:
            $sketchGrid["cols"] = "columns large-1 medium-2 small-3";
            $sketchGrid["row"] = 6;
            break;

    }
    return $sketchGrid;

}