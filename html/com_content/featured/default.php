<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');
$app    = JFactory::getApplication();
require(JPATH_SITE.'/templates/'.$app->getTemplate().'/scripts/sketchGrid.php');

JHtml::_('behavior.caption');

// If the page class is defined, add to class as suffix.
// It will be a separate class if the user starts it with a space
?>
<div class="blog-featured<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="http://schema.org/Blog">
    <?php if ($this->params->get('show_page_heading') != 0) : ?>
        <div class="page-header">
            <h1>
                <?php echo $this->escape($this->params->get('page_heading')); ?>
            </h1>
        </div>
    <?php endif; ?>

    <?php $leadingcount = 0; ?>
    <?php if (!empty($this->lead_items)) : ?>
        <div class="items-leading clearfix">
            <?php foreach ($this->lead_items as &$item) : ?>
                <div class="row">
                    <div
                        class="columns large-12 medium-12 small-12 leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?> clearfix"
                        itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                        <?php
                        $this->item = &$item;
                        echo $this->loadTemplate('item');
                        ?>
                    </div>
                </div>
                <?php
                $leadingcount++;
                ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php
    $introcount = (count($this->intro_items));

    ?>
    <?php if (!empty($this->intro_items)) :
        $sketch_row_count = 1;
$sketchGrid = getGridStructure($this->columns);
        $sketch_row = $sketchGrid["row"];
        $sketch_cols = $sketchGrid["cols"];
        ?>
        <div class="row <?php echo $sketch_row_count; ?>">
        <?php
        foreach ($this->intro_items as $key => &$item) : ?>




            <div class="<?php echo $sketch_cols; ?>">

                <div
                    class="item match <?php echo $item->state == 0 ? ' system-unpublished' : null; ?>"
                    itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
                    <?php
                    $this->item = &$item;
                    echo $this->loadTemplate('item');
                    ?>
                </div>
            </div>

            <?php if ($sketch_row_count %  $sketch_row== 0) { ?>
                </div>
                <div class="row"> <?php
            } ?>
            <?php $sketch_row_count++; ?>


        <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <?php if (!empty($this->link_items)) : ?>
        <div class="items-more">
            <?php echo $this->loadTemplate('links'); ?>
        </div>
    <?php endif; ?>

    <?php if ($this->params->def('show_pagination', 2) == 1 || ($this->params->get('show_pagination') == 2 && $this->pagination->pagesTotal > 1)) : ?>
        <div class="pagination">

            <?php if ($this->params->def('show_pagination_results', 1)) : ?>
                <p class="counter pull-right">
                    <?php echo $this->pagination->getPagesCounter(); ?>
                </p>
            <?php endif; ?>
            <?php echo $this->pagination->getPagesLinks(); ?>
        </div>
    <?php endif; ?>

</div>
