<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
?>
<div class="item-page reset<?php echo $this->pageclass_sfx ?>">
    <?php if ($this->params->get('show_page_heading')) : ?>
        <div class="row page-header">
            <div class="columns large-12">
                <h1>
                    <?php echo $this->escape($this->params->get('page_heading')); ?>
                </h1>
            </div>
        </div>
    <?php endif; ?>

    <form id="user-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request'); ?>"
          method="post" class="form-validate form-horizontal well">
        <?php foreach ($this->form->getFieldsets() as $fieldset) : ?>
            <div class="row">
                <div class="columns large-12">
                    <p><?php echo JText::_($fieldset->label); ?>
                    </p>
                </div>
            </div>
                <?php foreach ($this->form->getFieldset($fieldset->name) as $name => $field) : ?>
            <div class="row control-group">
                <div class="columns large-6 medium-12 small-12 control-label">
                    <?php echo $field->label; ?>
                </div>
                <div class="columns large-6 medium-12 small-12 controls">
                    <?php echo $field->input; ?>
                </div>
            </div>
        <?php endforeach; ?>
            </fieldset>
        <?php endforeach; ?>

        <div class="row control-group">
            <div class="columns large-12 ontrols">
                <button type="submit" class="btn btn-primary validate"><?php echo JText::_('JSUBMIT'); ?></button>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
