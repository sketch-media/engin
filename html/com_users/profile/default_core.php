<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>

<div class="row">
    <div class="columns large-12 legend">
        <?php echo JText::_('COM_USERS_PROFILE_CORE_LEGEND'); ?>
    </div>
</div>
<div class="row">
     <div class="columns large-6 medium-12 small-12 detail">
        <?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?>
    </div>
        <div class="columns large-6 medium-12 small-12 detail"> 
        <?php echo $this->data->name; ?>
    </div>
     <div class="columns large-6 medium-12 small-12 detail">
        <?php echo JText::_('COM_USERS_PROFILE_USERNAME_LABEL'); ?>
    </div>
        <div class="columns large-6 medium-12 small-12 detail"> 
        <?php echo htmlspecialchars($this->data->username); ?>
    </div>
     <div class="columns large-6 medium-12 small-12 detail">
        <?php echo JText::_('COM_USERS_PROFILE_REGISTERED_DATE_LABEL'); ?>
    </div>
        <div class="columns large-6 medium-12 small-12 detail"> 
        <?php echo JHtml::_('date', $this->data->registerDate); ?>
    </div>
     <div class="columns large-6 medium-12 small-12 detail">
        <?php echo JText::_('COM_USERS_PROFILE_LAST_VISITED_DATE_LABEL'); ?>
    </div>

    <?php if ($this->data->lastvisitDate != '0000-00-00 00:00:00') { ?>
            <div class="columns large-6 medium-12 small-12 detail"> 
            <?php echo JHtml::_('date', $this->data->lastvisitDate); ?>
        </div>
    <?php } else {
        ?>
            <div class="columns large-6 medium-12 small-12 detail"> 
            <?php echo JText::_('COM_USERS_PROFILE_NEVER_VISITED'); ?>
        </div>
    <?php } ?>

</div>
