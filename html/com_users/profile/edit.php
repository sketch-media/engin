<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

// Load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load('plg_user_profile', JPATH_ADMINISTRATOR);

?>
<div class="item-page profile-edit<?php echo $this->pageclass_sfx ?>">
    <?php if ($this->params->get('show_page_heading')) : ?>
        <div class="row page-header">
            <div class="columns large-12">
                <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
            </div>
        </div>
    <?php endif; ?>

    <script type="text/javascript">
        Joomla.twoFactorMethodChange = function (e) {
            var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

            jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function (i, el) {
                if (el.id != selectedPane) {
                    jQuery('#' + el.id).hide(0);
                }
                else {
                    jQuery('#' + el.id).show(0);
                }
            });
        }
    </script>

    <form id="member-profile" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save'); ?>"
          method="post" class="form-validate form-horizontal well" enctype="multipart/form-data">
        <?php // Iterate through the form fieldsets and display each one. ?>
        <?php foreach ($this->form->getFieldsets() as $group => $fieldset) : ?>
            <?php $fields = $this->form->getFieldset($group); ?>
            <?php if (count($fields)) : ?>
                <?php // If the fieldset has a label set, display it as the legend. ?>
                <?php if (isset($fieldset->label)) : ?>
                    <div class="row">
                        <div class="columns large-12"><legend>
                            <?php echo JText::_($fieldset->label); ?>
                            </legend>
                        </div>
                    </div>
                <?php endif; ?>
                <?php // Iterate through the fields in the set and display them. ?>
                <?php foreach ($fields as $field) : ?>
                    <?php // If the field is hidden, just display the input. ?>
                    <?php if ($field->hidden) : ?>
                        <?php echo $field->input; ?>
                    <?php else : ?>
                        <div class="row control-group">
                            <div class="columns large-6 medium-12 small-12 control-label">
                                <?php echo $field->label; ?>
                                <?php if (!$field->required && $field->type != 'Spacer') : ?>
                                    <span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="columns large-6 medium-12 small-12 controls">
                                <?php echo $field->input; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php endforeach; ?>

        <?php if (count($this->twofactormethods) > 1) : ?>
            <div class="row">
                <div
                    class="columns large-12 medium-12 small-12"> <?php echo JText::_('COM_USERS_PROFILE_TWO_FACTOR_AUTH'); ?>
                </div>
            </div>
            <div class="row control-group">
                <div class="columns large-12 medium-12 small-12 control-label">
                    <label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
                           title="<strong><?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_LABEL'); ?></strong><br /><?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_DESC'); ?>">
                        <?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_LABEL'); ?>
                    </label>
                </div>
                <div class="columns large-12 medium-12 small-12 controls">
                    <?php echo JHtml::_('select.genericlist', $this->twofactormethods, 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false); ?>
                </div>
            </div>
            <div class="row">
                <div class="columns large-12" id="com_users_twofactor_forms_container">
                    <?php foreach ($this->twofactorform as $form) : ?>
                        <?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
                        <div id="com_users_twofactor_<?php echo $form['method']; ?>" style="<?php echo $style; ?>">
                            <?php echo $form['form']; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="row">
                <div class="columns large-12">
                    <?php echo JText::_('COM_USERS_PROFILE_OTEPS'); ?>
                </div>
            </div>
            <div class="row">
                <div class="columns large-12 alert alert-info">
                    <?php echo JText::_('COM_USERS_PROFILE_OTEPS_DESC'); ?>
                </div>
            </div>
            <?php if (empty($this->otpConfig->otep)) : ?>

                <div class="row">
                    <div class="columns large-12 alert alert-warning">
                        <?php echo JText::_('COM_USERS_PROFILE_OTEPS_WAIT_DESC'); ?>
                    </div>
                </div>
            <?php else : ?>
                <?php foreach ($this->otpConfig->otep as $otep) : ?>
                    <div class="row">
                    <div class="columns large-12">
                        <span class=" span3
                    ">
                    <?php echo substr($otep, 0, 4); ?>-<?php echo substr($otep, 4, 4); ?>
                            -<?php echo substr($otep, 8, 4); ?>
                            -<?php echo substr($otep, 12, 4); ?>
                    </span>
                    </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
            <?php endif; ?>
            </div>
        <?php endif; ?>

        <div class="row control-group">
            <div class="columns large-12 controls">
                <button type="submit" class="btn btn-primary validate">
                    <span><?php echo JText::_('JSUBMIT'); ?></span>
                </button>
                <a class="btn" href="<?php echo JRoute::_(''); ?>"
                   title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
                <input type="hidden" name="option" value="com_users"/>
                <input type="hidden" name="task" value="profile.save"/>
            </div>
        </div>
        <?php echo JHtml::_('form.token'); ?>
    </form>
</div>
