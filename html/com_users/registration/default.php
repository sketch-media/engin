<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
?>
<div class="item-page registration">
    <div class="row registration<?php echo $this->pageclass_sfx ?>">

        <?php if ($this->params->get('show_page_heading')) : ?>
            <div class="columns large-12 page-header">
                <div class="box">
                    <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="registration-form">

        <form id="member-registration"
              action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post"
              class="form-validate form-horizontal well" enctype="multipart/form-data">
            <?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.     ?>
            <?php $fields = $this->form->getFieldset($fieldset->name); ?>
            <?php if (count($fields)): ?>


            <div class="row control-group">
                <?php $i = 1;
                foreach ($fields as $field) :// Iterate through the fields in the set and display them.
                    ?>
                    <?php if ($field->hidden):// If the field is hidden, just display the input.?>
                    <div class="columns large-6 medium-12 small-12 control-label">
                        <?php echo $field->input; ?>
                    </div>
                <?php else: ?>
                    <div class="columns large-6 medium-12 small-12 control-label">
                        <?php print_r($field->label); ?>
                        <?php if (!$field->required && $field->type != 'Spacer') : ?>
                            <span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="columns large-6 medium-12 small-12 controls">
                        <?php echo $field->input; ?>
                    </div>
                <?php endif;

                    if ($i % 1 == 0) {

                        echo "</div><div class='row control-group'>";

                    }
                    $i++;

                endforeach; ?>
            </div>

    </div>
    <?php endif; ?>
    <?php endforeach; ?>
    <div class="row control-group">
        <div class="columns large-12 controls">
            <button type="submit" class="btn btn-primary validate"><?php echo JText::_('JREGISTER'); ?></button>
            <a class="btn" href="<?php echo JRoute::_(''); ?>"
               title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>
            <input type="hidden" name="option" value="com_users"/>
            <input type="hidden" name="task" value="registration.register"/>
        </div>
    </div>
    <?php echo JHtml::_('form.token'); ?>
    </form>
</div>