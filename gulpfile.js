var gulp = require('gulp'),
	gulpLoadPlugins = require('gulp-load-plugins'),
	plugins = gulpLoadPlugins();
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
// styles
gulp.task('sass', function() {

	return sass('sass/styles.scss',{
		noCache: true,
		sourcemap: true
	})
		.pipe(sourcemaps.init())
		.pipe(plugins.cssnano({zindex:false}))
		.pipe(plugins.autoprefixer())
		.pipe(sourcemaps.write('../stylesheets'))
		.pipe(gulp.dest('stylesheets'));
});

// scripts
gulp.task('scripts', function() {
	return gulp.src('javascript/*.js')
		.pipe(plugins.uglify())
		.pipe(plugins.concat('general.min.js'))
		.pipe(gulp.dest('scripts'));
});
// images
gulp.task('images', function() {
	return gulp.src('images/source/**/*')
		.pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
		.pipe(gulp.dest('images'));
});
// watch files for changes
gulp.task('watch', function() {
	gulp.watch('sass/*.scss', ['sass']);
	gulp.watch('javascript/*.js', ['scripts']);
	gulp.watch('images/source/**/*', ['images']);
});
// default task
gulp.task('default', ['watch']);