<?php
defined('_JEXEC') or die;
JHTML::_('jquery.framework');
$this->setGenerator(null);
$app = JFactory::getApplication();
$option = $app->input->getCmd('option', '');
$menu = $app->getMenu()->getActive();
if ($menu) {
	$pageclass = $menu->params->get('pageclass_sfx');
}
$langTag = JFactory::getApplication()->getLanguage()->getTag();
$tpar = $this->params;
?>
<!DOCTYPE html>
<html lang="<?php echo $langTag; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<jdoc:include type="head"/>
	<link rel="stylesheet"
	      href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/stylesheets/styles.css"
	      type="text/css"/>
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:400,700" rel="stylesheet">
</head>
<body id="<?php echo $option; ?>" class="<?php echo htmlspecialchars($pageclass); ?>">
<div class="allwrap">
	<header class="sketchwrap_top">
		<div class="row">
			<div class="columns xlarge-12 large-12 medium-12 small-12">
			</div>
		</div>
		<div class="row">
			<div class="headerwrap box">
				<div class="xlarge-4 large-12 medium-12 small-12 columns">
					<a id="main-nav" href="#sidr-main"
					   class="icon-menu" <?php if ($tpar->get("sidrpos", 1) == 0) { ?> style="float: right;" <?php } ?>></a>
					<div class="top_left">
						<jdoc:include type="modules" name="top_left" style="html5"/>
					</div>
				</div>
				<div class="xlarge-8 large-12 medium-12 small-12 columns">
					<div class="top_right">
						<jdoc:include type="modules" name="top_right" style="html5"/>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<div class="menuwrap box">
					<nav class="top_menu">
						<jdoc:include type="modules" name="top_menu" style="html5"/>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<div class="sketchwrap_slider">
		<?php if ($tpar->get("sliderwidth", 1) == 0){ ?>
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<?php } ?>
				<jdoc:include type="modules" name="slider" style="html5"/>
				<?php if ($tpar->get("sliderwidth", 1) == 0){ ?>
			</div>
		</div>
	<?php } ?>
	</div>
	<main>
		<?php if($this->countModules('breadcrumbs') ) { ?>
		<div class="breadcrumb_wrap">
			<div class="row">
				<div class="columns xlarge-12 large-12 medium-12 small-12">
					<jdoc:include type="modules" name="breadcrumbs" style="html5"/>
				</div>
			</div>
		</div>
		<?php } ?>
		<div class="sketchwrap_content_top">
			<?php if ($tpar->get("ct_2", 0) == 1) { ?>
				<div class="row">
					<div class="large-6 medium-12 small-12 columns">
						<div class="content_top_1_2">
							<jdoc:include type="modules" name="content_top_1_2" style="html5"/>
						</div>
					</div>
					<div class="large-6 medium-12 small-12 columns">
						<div class="content_top_2_2">
							<jdoc:include type="modules" name="content_top_2_2" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if ($tpar->get("ct_3", 0) == 1) { ?>
				<div class="row">
					<div class="large-4 medium-6 small-12 columns">
						<div class="content_top_1_3">
							<jdoc:include type="modules" name="content_top_1_3" style="html5"/>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 columns">
						<div class="content_top_2_3">
							<jdoc:include type="modules" name="content_top_2_3" style="html5"/>
						</div>
					</div>
					<div class="large-4 medium-12 small-12 columns">
						<div class="content_top_2_3">
							<jdoc:include type="modules" name="content_top_3_3" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if ($tpar->get("ct_4", 0) == 1) { ?>
				<div class="row">
					<div class="large-3 medium-6 small-12 columns">
						<div class="content_top_1_4">
							<jdoc:include type="modules" name="content_top_1_4" style="html5"/>
						</div>
					</div>
					<div class="large-3 medium-6 small-12 columns">
						<div class="content_top_2_4">
							<jdoc:include type="modules" name="content_top_2_4" style="html5"/>
						</div>
					</div>
					<div class="large-3 medium-6 small-12 columns">
						<div class="content_top_3_4">
							<jdoc:include type="modules" name="content_top_3_4" style="html5"/>
						</div>
					</div>
					<div class="large-3 medium-6 small-12 columns">
						<div class="content_top_4_4">
							<jdoc:include type="modules" name="content_top_4_4" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<section class="sketchwrap_content_main">
			<div class="row">
				<div class="large-12 columns">
					<div class="row">
						<?php if ($this->countModules('content_main_right') && !$this->countModules('content_main_left')) { ?>
							<div class="xlarge-9 large-12 medium-12 small-12 columns">
								<div class="content_main_top">
									<jdoc:include type="modules" name="content_main_top" style="html5"/>
								</div>
								<div class="content_main">
									<jdoc:include type="message"/>
									<jdoc:include type="component" style="html5"/>
								</div>
								<div class="content_main_bottom">
									<jdoc:include type="modules" name="content_main_bottom" style="html5"/>
								</div>
							</div>
							<div class="xlarge-3 large-12 medium-12 small-12 columns">
								<div class="content_main_right">
									<jdoc:include type="modules" name="content_main_right" style="html5"/>
								</div>
							</div>
						<?php } elseif ($this->countModules('content_main_left') && !$this->countModules('content_main_right')) {
							?>
							<div class="xlarge-3 large-12 medium-12 small-12 columns">
								<div class="content_main_left">
									<jdoc:include type="modules" name="content_main_left" style="html5"/>
								</div>
							</div>
							<div class="xlarge-9 large-12 medium-12 small-12 columns">
								<div class="content_main_top">
									<jdoc:include type="modules" name="content_main_top" style="html5"/>
								</div>
								<div class="content_main">
									<jdoc:include type="message"/>
									<jdoc:include type="component" style="html5"/>
								</div>
								<div class="content_main_bottom">
									<jdoc:include type="modules" name="content_main_bottom" style="html5"/>
								</div>
							</div>
							<?php
						} elseif ($this->countModules('content_main_left') && $this->countModules('content_main_right')) {
							?>
							<div class="xlarge-3 large-12 medium-12 small-12 columns">
								<div class="content_main_left">
									<jdoc:include type="modules" name="content_main_left" style="html5"/>
								</div>
							</div>
							<div class="xlarge-6 large-12 medium-12 small-12 columns">
								<div class="content_main_top">
									<jdoc:include type="modules" name="content_main_top" style="html5"/>
								</div>
								<div class="content_main">
									<jdoc:include type="message"/>
									<jdoc:include type="component" style="html5"/>
								</div>
								<div class="content_main_bottom">
									<jdoc:include type="modules" name="content_main_bottom" style="html5"/>
								</div>
							</div>
							<div class="xlarge-3 large-12 medium-12 small-12 columns">
								<div class="content_main_right">
									<jdoc:include type="modules" name="content_main_right" style="html5"/>
								</div>
							</div>
							<?php
						} else { ?>
							<div class="xlarge-12 large-12 medium-12 small-12 columns">
								<div class="content_main_top">
									<jdoc:include type="modules" name="content_main_top" style="html5"/>
								</div>
								<div class="content_main">
									<jdoc:include type="message"/>
									<jdoc:include type="component" style="html5"/>
								</div>
								<div class="content_main_bottom">
									<jdoc:include type="modules" name="content_main_bottom" style="html5"/>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</section>
		<div class="sketchwrap_content_bottom">
			<?php if ($tpar->get("cb75_2", 0) == 1) { ?>
				<div class="row">
					<div class="large-9 medium-12 columns">
						<div class="content_bottom_1_2">
							<jdoc:include type="modules" name="content_bottom_1_2" style="html5"/>
						</div>
					</div>
					<div class="large-3 medium-12 columns">
						<div class="content_bottom_2_2">
							<jdoc:include type="modules" name="content_bottom_2_2" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if ($tpar->get("cb50_2", 0) == 1) { ?>
				<div class="row">
					<div class="large-6 medium-6 small-12 columns">
						<div class="content_bottom50_1_2">
							<jdoc:include type="modules" name="content_bottom_1_2" style="html5"/>
						</div>
					</div>
					<div class="large-6 medium-6 small-12 columns">
						<div class="content_bottom50_2_2">
							<jdoc:include type="modules" name="content_bottom_2_2" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if ($tpar->get("cb_3", 0) == 1) { ?>
				<div class="row">
					<div class="large-4 medium-6 small-12 columns">
						<div class="content_bottom_1_3">
							<jdoc:include type="modules" name="content_bottom_1_3" style="html5"/>
						</div>
					</div>
					<div class="large-4 medium-6 small-12 columns">
						<div class="content_bottom_2_3">
							<jdoc:include type="modules" name="content_bottom_2_3" style="html5"/>
						</div>
					</div>
					<div class="large-4 medium-12 small-12 columns">
						<div class="content_bottom_3_3">
							<jdoc:include type="modules" name="content_bottom_3_3" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
			<?php if ($tpar->get("cb_4", 0) == 1) { ?>
				<div class="row">
					<div class="xlarge-3 large-6 medium-6 small-12 columns">
						<div class="content_bottom_1_4">
							<jdoc:include type="modules" name="content_bottom_1_4" style="html5"/>
						</div>
					</div>
					<div class="xlarge-3 large-6 medium-6 small-12 columns">
						<div class="content_bottom_2_4">
							<jdoc:include type="modules" name="content_bottom_2_4" style="html5"/>
						</div>
					</div>
					<div class="xlarge-3 large-6 medium-6 small-12 columns">
						<div class="content_bottom_3_4">
							<jdoc:include type="modules" name="content_bottom_3_4" style="html5"/>
						</div>
					</div>
					<div class="xlarge-3 large-6 medium-6 small-12 columns">
						<div class="content_bottom_4_4">
							<jdoc:include type="modules" name="content_bottom_4_4" style="html5"/>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</main>
</div>
<?php if($this->countModules('maps')){ ?>
	<div class="sketchwrap_template_maps">
		<jdoc:include type="modules" name="maps" style="html5"/>
	</div>
<?php } ?>
<footer>
	<div class="row">
		<div class="xlarge-3 large-6 medium-6 small-12 columns">
			<div class="content_bottom_1_4">
				<jdoc:include type="modules" name="bottom_1_4" style="html5"/>
			</div>
		</div>
		<div class="xlarge-3 large-6 medium-6 small-12 columns">
			<div class="content_bottom_2_4">
				<jdoc:include type="modules" name="bottom_2_4" style="html5"/>
			</div>
		</div>
		<div class="xlarge-3 large-6 medium-6 small-12 columns">
			<div class="content_bottom_3_4">
				<jdoc:include type="modules" name="bottom_3_4" style="html5"/>
			</div>
		</div>
		<div class="xlarge-3 large-6 medium-6 small-12 columns">
			<div class="content_bottom_4_4">
				<jdoc:include type="modules" name="bottom_4_4" style="html5"/>
			</div>
		</div>
	</div>
	<div class="sketchwrap_bottom">
		<div class="row">
			<div class="large-8 medium-12 small-12 columns">
				<nav class="bottom">
					<jdoc:include type="modules" name="footer" style="html5"/>
				</nav>
			</div>
			<div class="large-4 medium-12 small-12 columns">
				<div class="footer_copy">Engin's Grill &copy; <?php echo date("Y"); ?></div>
			</div>
		</div>
	</div>
</footer>
<jdoc:include type="modules" name="debug"/>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/scripts/general.min.js"
        type="text/javascript"></script>
<script>
	jQuery('#main-nav').sidr(
		{
			name: 'sidr-main',
			source: '.slidenav1, .slidenav2, .slidenav3, .slidenav4'<?php if ($tpar->get("sidrpos", 1) == 0) {
			?>,
			side: "right" <?php
			} ?>
		}
	);
	jQuery("div.sidr-inner:contains('undefined')").hide();
	// MatchHeight
	jQuery(function () {
		jQuery('.match').matchHeight();
	});
</script>
<script>
	var gaProperty = 'UA-106624108-1';
	var disableStr = 'ga-disable-' + gaProperty;
	if (document.cookie.indexOf(disableStr + '=true') > -1) {
		window[disableStr] = true;
	}
	function gaOptout() {
		document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
		window[disableStr] = true;
	}
</script>
<script>
	(function (i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
		a = s.createElement(o),
			m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
	ga('create', 'UA-106624108-1', 'auto');
	ga('set', 'anonymizeIp', true);
	ga('send', 'pageview');
</script>
</body>
</html>
