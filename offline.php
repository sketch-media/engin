<?php
defined('_JEXEC') or die;
JHTML::_('behavior.framework', true);
$app = JFactory::getApplication();
$tplparams	= $app->getTemplate(true)->params;
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <jdoc:include type="head"/>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/stylesheets/styles.css" type="text/css"/>

 <!-- Google Fonts -->
 
</head>
<body>
<div class="allwrap">
    <header>
        <div class="row">
            <div class="large-6 columns">
                <div class="logo">
                </div>
            </div>

            <div class="large-6 columns">

            </div>
        </div>
    </header>
    <div id="slider">
        <div class="row">
            <div class="large 12 columns">
            </div>
        </div>
    </div>
    <section class="content">
        <div class="row">
            <div class="large-12 columns">
            
                   <?php echo htmlspecialchars($app->getCfg('sitename')); ?>
    </h1><?php if ($app->getCfg('display_offline_message', 1) == 1 && str_replace(' ', '', $app->getCfg('offline_message')) != ''): ?>
        <p><?php echo $app->getCfg('offline_message'); ?></p>
    <?php elseif ($app->getCfg('display_offline_message', 1) == 2 && str_replace(' ', '', JText::_('JOFFLINE_MESSAGE')) != ''): ?>
        <p><?php echo JText::_('JOFFLINE_MESSAGE'); ?></p>
    <?php endif; ?>
                        <div class="login-form_offline" > 
    <form action="<?php echo JRoute::_('index.php', true); ?>" method="post" name="login" id="form-login">
      <fieldset class="input">
        <p id="form-login-username">
          <label for="username"><?php echo JText::_('JGLOBAL_USERNAME'); ?></label><br />
          <input type="text" name="username" id="username" class="inputbox" alt="<?php echo JText::_('JGLOBAL_USERNAME'); ?>" size="18" />
        </p>
        <p id="form-login-password">
          <label for="passwd"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label><br />
          <input type="password" name="password" id="password" class="inputbox" alt="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" size="18" />
        </p>
        <p id="form-login-remember">
          <label for="remember"><?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?></label>
          <input type="checkbox" name="remember" value="yes" alt="<?php echo JText::_('JGLOBAL_REMEMBER_ME'); ?>" id="remember" />
        </p>
        <p id="form-login-submit">
          <label>&nbsp;</label>
          <input type="submit" name="Submit" class="button" value="<?php echo JText::_('JLOGIN'); ?>" />
        </p>
      </fieldset>
      <input type="hidden" name="option" value="com_users" />
      <input type="hidden" name="task" value="user.login" />
      <input type="hidden" name="return" value="<?php echo base64_encode(JURI::base()); ?>" />
      <?php echo JHTML::_( 'form.token' ); ?>
    </form></div>

             
             
             
             
             
            </div>
        </div>
    </section>
    <footer>
        <div class="row">
            <div class="large-12 columns">
            </div>
        </div>
    </footer>

</div>


<jdoc:include type="modules" name="debug"/>

<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/scripts/general.min.js" type="text/javascript"></script>

</body>

</html>
