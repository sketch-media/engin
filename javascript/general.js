jQuery(document).ready(function () {
	//sidr subtoggle
	jQuery('.sidr-class-parent:not(.sidr-class-active) ul').slideToggle().hide();
	jQuery('.sidr-class-parent').prepend("<span class='toggl0r'>></span>");
	jQuery('.sidr-class-parent > span.toggl0r').click(function () {
		jQuery(this).siblings('ul').slideToggle();
		return false;
	});
	//sidr close on resize
	window.onresize = function () {
		jQuery.sidr('close', 'sidr-main');
	};
	// close alerts
	jQuery("a.close").click(function () {
		jQuery(this).parent().hide();
	});
	// responsive Dropdown
	jQuery(".nav.menu li:has(ul)").doubleTapToGo();
	// German Image Title
	if (jQuery("html").attr("lang") == "de-DE") {
		jQuery(".sigProContainer img").each(function () {
			jQuery(this).attr("title", "Zum Vergrößern klicken");
		});
	}
});



