<?php
defined('_JEXEC') or die;
JHTML::_('behavior.framework', true);
$app = JFactory::getApplication();
$tplparams = $app->getTemplate(true)->params;
?>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet"
	      href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/stylesheets/styles.css"
	      type="text/css"/>
	<!-- Google Fonts -->
</head>
<body>
<div class="allwrap">
	<header>
		<div class="row">
			<div class="large-6 columns">
				<div class="logo">
				</div>
			</div>
	</header>
	<main>
	<div class="content_main">
		<div class="row">
			<div class="large-12 columns">
				<div class="item-page">
					<div class="error">
						<div id="outline">
							<div id="errorboxoutline">
								<div id="errorboxheader"><?php echo $this->error->getCode(); ?>
									- <?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></div>
								<div id="errorboxbody">
									<p><strong><?php echo JText::_('JERROR_LAYOUT_NOT_ABLE_TO_VISIT'); ?></strong></p>
									<ol>
										<li><?php echo JText::_('JERROR_LAYOUT_AN_OUT_OF_DATE_BOOKMARK_FAVOURITE'); ?></li>
										<li><?php echo JText::_('JERROR_LAYOUT_SEARCH_ENGINE_OUT_OF_DATE_LISTING'); ?></li>
										<li><?php echo JText::_('JERROR_LAYOUT_MIS_TYPED_ADDRESS'); ?></li>
										<li><?php echo JText::_('JERROR_LAYOUT_YOU_HAVE_NO_ACCESS_TO_THIS_PAGE'); ?></li>
										<li><?php echo JText::_('JERROR_LAYOUT_REQUESTED_RESOURCE_WAS_NOT_FOUND'); ?></li>
										<li><?php echo JText::_('JERROR_LAYOUT_ERROR_HAS_OCCURRED_WHILE_PROCESSING_YOUR_REQUEST'); ?></li>
									</ol>
									<p>
										<strong><?php echo JText::_('JERROR_LAYOUT_PLEASE_TRY_ONE_OF_THE_FOLLOWING_PAGES'); ?></strong>
									</p>
									<div id="techinfo">
										<p><?php echo htmlspecialchars($this->error->getMessage(), ENT_QUOTES, 'UTF-8'); ?></p>
										<p>
											<?php if ($this->debug) :
												echo $this->renderBacktrace();
											endif; ?>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</main>
	<footer>
		<div class="row">
			<div class="large-12 columns">
			</div>
		</div>
	</footer>
</div>
<script src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/scripts/general.min.js"
        type="text/javascript"></script>
</body>
</html>
